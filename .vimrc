
set nocompatible
execute pathogen#infect()

syntax on
filetype plugin indent on

let g:jellybeans_overrides = {
\    'background': { 'ctermbg': 'none', '256ctermbg': 'none' },
\}
let g:jellybeans_use_term_italics = 1
colorscheme jellybeans

let $PAGER=''

" Options {{{
set noequalalways
set hidden
set bufhidden=hide
set expandtab
set number
set relativenumber
set autoindent
set cindent
set nowrap
set hlsearch
set incsearch

set laststatus=2
set tabstop=4
set softtabstop=4
set shiftwidth=4
set numberwidth=4
set textwidth=120
set laststatus=2
set t_Co=256
set cpoptions+=J
set rtp+=~/.local/lib64/python3.6/site-packages/powerline/bindings/vim

set grepprg=grep\ -nH\ $*
" }}}

" Plugin global state {{{
let g:tex_flavor = "latex"
let g:powerline_pycmd = "py3"
let g:snips_author = "Szymon Walter"
let g:clam_winwidth = 120
let g:gundo_preview_bottom = 1
let g:ft_man_open_mode = 'tab'
let g:racer_cmd = "~/.cargo/bin/racer"
let g:racer_experimental_completer = 1
let g:rustfmt_command = "~/.cargo/bin/rustfmt"
let g:rustfmt_autosave = 1
let g:rust_recommended_style = 0

let g:fireplace_no_maps = 1
let g:paredit_shortmaps = 0
let g:paredit_smartjump = 0
let g:paredit_leader = "\\"
let g:ConqueTerm_Color = 2
let g:ConqueTerm_ReadUnfocused = 1
let g:ConqueTerm_TERM = 'xterm'
" }}}

" <Leader> = <space>, <LocalLeader> = \ {{{
let mapleader = " "
let maplocalleader = "\\"
" }}}

" colemak {{{
function! Colemak()
    noremap n h
    noremap e j
    noremap i k
    noremap o l
    
    noremap <C-W>n <C-W>h
    noremap <C-W>e <C-W>j
    noremap <C-W>i <C-W>k
    noremap <C-W>o <C-W>l
    
    noremap <S-N> <S-H>
    noremap <S-E> <S-J>
    noremap <S-I> <S-K>
    noremap <S-O> <S-L>
    
    noremap <C-N> <C-H>
    noremap <C-E> <C-J>
    noremap <C-I> <C-K>
    noremap <C-O> <C-L>
    
    noremap h n
    noremap j e
    noremap k i
    noremap l o
    
    noremap <C-W>h <C-W>n
    noremap <C-W>j <C-W>e
    noremap <C-W>k <C-W>i
    noremap <C-W>l <C-W>o
    
    noremap <S-H> <S-N>
    noremap <S-J> <S-E>
    noremap <S-K> <S-I>
    noremap <S-L> <S-O>
    
    noremap <C-H> <C-N>
    noremap <C-J> <C-E>
    noremap <C-K> <C-I>
    noremap <C-L> <C-O>

    nunmap <Leader>ev
    nnoremap <Leader>mv :split $MYVIMRC<CR>
    nnoremap <Leader>i :bp<CR>
    nnoremap <Leader>e :bn<CR>

    cunmap <C-H>
    cunmap <C-J>
    cunmap <C-K>
    cunmap <C-L>

    cnoremap <C-N> <Left>
    cnoremap <C-E> <Down>
    cnoremap <C-I> <Up>
    cnoremap <C-O> <Right>

    nnoremap <TAB> :ls<CR>:b
endfunction
" }}}

" semi-interactive clojure in vim {{{
command! -bar Clj call Clj()

function! Clj()
    execute "normal! \<C-W>v\<C-W>l"
    edit Repl
    set buftype=nowrite
    set filetype=clojure
    Console
endfunction
" }}}

" Insert mode abbrevs {{{
iabbrev @@ walter.szymon.98@gmail.com
iabbrev wls@ wls@driftinginyellow.tk
iabbrev diytk driftinginyellow.tk
iabbrev ssig Szymon Walter<CR>walter.szymon.98@gmail.com
" }}}

" autocmd {{{
"autocmd BufRead *.rs :setlocal filetype=rust
autocmd BufRead *.lalrpop :setlocal filetype=rust
autocmd BufRead *.inc :setlocal filetype=asm
" }}}

" FileType configuration {{{
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
    autocmd FileType vim iabbrev <buffer> > >#
    autocmd FileType vim iabbrev <buffer> >= >=#
    autocmd FileType vim iabbrev <buffer> < <#
    autocmd FileType vim iabbrev <buffer> <= <=#
    autocmd FileType vim iabbrev <buffer> == ==#
    autocmd FileType vim iabbrev <buffer> != !=#
augroup END

augroup filetype_asm
    autocmd!
    autocmd FileType asm set filetype=nasm
augroup END

augroup filetype_c
    autocmd!
    autocmd FileType c nnoremap <LocalLeader>c 0i// <ESC>
    autocmd FileType c nnoremap <LocalLeader>x 03x
    autocmd FileType cpp nnoremap <LocalLeader>c 0i// <ESC>
    autocmd FileType cpp nnoremap <LocalLeader>x 03x
augroup END

augroup filetype_rust
    autocmd!
    autocmd FileType rust nnoremap <LocalLeader>c 0i// <ESC>
    autocmd FileType rust nnoremap <LocalLeader>x 03x
    autocmd FileType rust nmap gd <Plug>(rust-def)
    autocmd FileType rust nmap gs <Plug>(rust-def-split)
    autocmd FileType rust nmap gx <Plug>(rust-def-vertical)
    autocmd FileType rust nmap <leader>gd <Plug>(rust-doc)
augroup END

augroup filetype_lua
    autocmd!
    autocmd FileType lua nnoremap <LocalLeader>c 0i-- <ESC>
    autocmd FileType lua nnoremap <LocalLeader>x 03x
augroup END

augroup filetype_python
    autocmd!
    autocmd FileType python nnoremap <LocalLeader>c 0i# <ESC>
    autocmd FileType python nnoremap <LocalLeader>x 02x
augroup END

augroup filetype_sh
    autocmd!
    autocmd FileType sh nnoremap <LocalLeader>c 0i# <ESC>
    autocmd FileType sh nnoremap <LocalLeader>x 02x
augroup END

augroup filetype_html
    autocmd!
    autocmd FileType html nnoremap <LocalLeader>c 0i<!--<ESC>A><ESC>
    autocmd FileType html nnoremap <LocalLeader>x 04x$x
augroup END

augroup filetype_md
    autocmd!
    autocmd FileType md setlocal spell spelllang=en_gb
augroup END

augroup filetype_tex
    autocmd!
    autocmd FileType tex nnoremap <LocalLeader>c 0i% <ESC>
    autocmd FileType tex nnoremap <LocalLeader>x 02x
    " refresh vim-latex folds
    autocmd FileType tex nnoremap <LocalLeader>rf <Plug>Tex_RefreshFolds
    autocmd FileType tex setlocal spell spelllang=en_gb
    autocmd FileType tex setlocal makeprg=make
    autocmd FileType tex onoremap p i{
augroup END

augroup filetype_clj
    autocmd!
    autocmd FileType clojure nmap <LocalLeader>r :%Eval<CR>

    autocmd FileType clojure nmap <LocalLeader>K <Plug>FireplaceK
    autocmd FileType clojure nmap <LocalLeader>s <Plug>FireplaceSource
    autocmd FileType clojure nmap cpp <Plug>FireplaceCountPrint

    autocmd FileType clojure nmap cm <Plug>FireplaceMacroExpand
    autocmd FileType clojure nmap c1mm <Plug>FireplaceCount1MacroExpand

    autocmd FileType clojure nmap cqq <Plug>FireplaceCountEdit

    autocmd FileType clojure nmap cqp <Plug>FireplacePrompt
    autocmd FileType clojure nmap cqc <Plug>FireplacePrompt

    autocmd FileType clojure imap <C-R>( <Plug>FireplaceRecall
    autocmd FileType clojure cmap <C-R>( <Plug>FireplaceRecall
    autocmd FileType clojure smap <C-R>( <Plug>FireplaceRecall

    autocmd FileType clojure nmap gf <Plug>FireplaceEditFile
    autocmd FileType clojure nmap <C-W>f <Plug>FireplaceSplitFile
    autocmd FileType clojure nmap <C-W>gf <Plug>FireplaceTabeditFile
    autocmd FileType clojure nmap <LocalLeader>D <Plug>FireplaceDjump
augroup END

augroup filetype_lpi
     autocmd!
     autocmd! FileType lispi map <LocalLeader>r :! rustytone file <C-R>%<CR>
augroup END
" }}}

"" Hexmode taken from vim wikia: http://vim.wikia.com/wiki/Improved_Hex_editing
" {{{
" ex command for toggling hex mode - define mapping if desired
command! -bar Hexmode call ToggleHex()

" helper function to toggle hex mode
function! ToggleHex()
  " hex mode should be considered a read-only operation
  " save values for modified and read-only for restoration later,
  " and clear the read-only flag for now
  let l:modified=&mod
  let l:oldreadonly=&readonly
  let &readonly=0
  let l:oldmodifiable=&modifiable
  let &modifiable=1
  if !exists("b:editHex") || !b:editHex
    " save old options
    let b:oldft=&ft
    let b:oldbin=&bin
    " set new options
    setlocal binary " make sure it overrides any textwidth, etc.
    silent :e " this will reload the file without trickeries 
              "(DOS line endings will be shown entirely )
    let &ft="xxd"
    " set status
    let b:editHex=1
    " switch to hex editor
    %!xxd
  else
    " restore old options
    let &ft=b:oldft
    if !b:oldbin
      setlocal nobinary
    endif
    " set status
    let b:editHex=0
    " return to normal editing
    %!xxd -r
  endif
  " restore values for modified and read only state
  let &mod=l:modified
  let &readonly=l:oldreadonly
  let &modifiable=l:oldmodifiable
endfunction
" }}}

" netrw config {{{
augroup netrw_mapping
    autocmd!
    autocmd filetype netrw call NetrwMapping()
augroup END

nnoremap <leader>ls :Vexplore .<CR>
nnoremap <leader>xl :1wincmd c<CR>

let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 12

 augroup ProjectDrawer
   autocmd!
   autocmd VimEnter * :Vexplore
   autocmd VimEnter * :set hidden
   autocmd BufRead * :set hidden
 augroup END

function! NetrwMapping()
    noremap <buffer> n h
    noremap <buffer> e j
    noremap <buffer> i k
    noremap <buffer> o l
    
    noremap <buffer> <C-W>n <C-W>h
    noremap <buffer> <C-W>e <C-W>j
    noremap <buffer> <C-W>i <C-W>k
    noremap <buffer> <C-W>o <C-W>l
    
    noremap <buffer> <S-N> <S-H>
    noremap <buffer> <S-E> <S-J>
    noremap <buffer> <S-I> <S-K>
    noremap <buffer> <S-O> <S-L>
    
    noremap <buffer> <C-N> <C-H>
    noremap <buffer> <C-E> <C-J>
    noremap <buffer> <C-I> <C-K>
    noremap <buffer> <C-O> <C-L>
    
    noremap <buffer> h n
    noremap <buffer> j e
    noremap <buffer> k i
    noremap <buffer> l o
    
    noremap <buffer> <C-W>h <C-W>n
    noremap <buffer> <C-W>j <C-W>e
    noremap <buffer> <C-W>k <C-W>i
    noremap <buffer> <C-W>l <C-W>o
    
    noremap <buffer> <S-H> <S-N>
    noremap <buffer> <S-J> <S-E>
    noremap <buffer> <S-K> <S-I>
    noremap <buffer> <S-L> <S-O>
    
    noremap <buffer> <C-H> <C-N>
    noremap <buffer> <C-J> <C-E>
    noremap <buffer> <C-K> <C-I>
    noremap <buffer> <C-L> <C-O>
endfunction
" }}}

" cquery {{{
let g:lsc_server_commands = {
  \ 'c': 'cquery --init="{\"cacheDirectory\": \"/tmp/cquery_cache\"}"',
  \ 'cpp': 'cquery --init="{\"cacheDirectory\": \"/tmp/cquery_cache\"}"',
  \ }
" }}}

" Custom key mappings {{{
" maps {{{
noremap <F2> :retab<CR>
noremap <F3> :set noet<CR>
noremap <S-F3> :set et<CR>
noremap / /\v
noremap ? ?\v

noremap <Leader>sh :sh<CR>
" perl subst
noremap <Leader>su :perldo s/
" very magic subst
noremap <Leader>vs :%s/\v
"}}}

" nmaps {{{
" edit vimrc
nnoremap <Leader>ev :split $MYVIMRC<CR>
" source vimrc
nnoremap <Leader>sv :source $MYVIMRC<CR>
nnoremap <Leader>mk :make<CR>
" exec a shell command
nnoremap <Leader>: :Clam 
" semicolon
nnoremap <Leader>sc mmA;<ESC>`m
" split
nnoremap <Leader>sp :execute "leftabove split" bufname("#")<CR>
" Upper
nnoremap <Leader><S-U> viw<S-U>
" trim whitespace
nnoremap <Leader>tw :%s/\v\s+$//g<CR>
" lookup word
nnoremap <Leader>lw :OnlineThesaurusLookup<CR>
" undo
nnoremap <Leader>u :GundoToggle<CR>
" rm current file
nnoremap <Leader>rm :Clam rm <C-R>%<CR>:q<CR>:bd<CR>
" re-open current file
nnoremap <Leader>ro :bd<CR>:e <C-R>#<CR>
" open last buffer
nnoremap <Leader>bp :e <C-R>#<CR>
nnoremap <Leader>k :bp<CR>
nnoremap <Leader>j :bn<CR>
nnoremap <Leader>/ :noh<CR>
nnoremap <Leader>1 :set rnu!<CR>
nnoremap <C-Z> <NOP>
nnoremap <CR> o<ESC>
nnoremap <TAB> :ls<CR>:b
nnoremap <LEADER><TAB> :reg<CR>

nnoremap <Leader>vr <C-X><C-O>

nnoremap <Leader>ct :ConqueTerm zsh<CR>
nnoremap <Leader>cg :ConqueGdb<CR>
" }}}

" vmaps {{{
vnoremap <Leader>q <ESC>`>a"<ESC>`<i"<ESC>`>l
" }}}

" imaps {{{
inoremap <C-U> <ESC>viw<S-U>i
" }}}

" cmaps {{{
cnoremap <C-H> <Left>
cnoremap <C-J> <Down>
cnoremap <C-K> <Up>
cnoremap <C-L> <Right>
" }}}

" some custom motions {{{
" }}}
" }}}

call Colemak()
nohlsearch
